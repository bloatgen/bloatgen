package main

import (
	"fmt"
	"html/template"
	"path"
)

func ReadTemplate() *template.Template {
	fmt.Println("Reading template")
	tpl, err := template.ParseGlob(path.Join(cfg.dirs.template, "*.tpl"))
	if err != nil {
		fail(" - error: %s\n", err)
	}
	return tpl
}
