package main

import (
	"bufio"
	"fmt"
	"github.com/russross/blackfriday"
	"html/template"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
)

var (
	errMeta        = fmt.Errorf("Failed to parse metadata.")
	errVarNotExist = fmt.Errorf("Variable does not exist")
)

type Content struct {
	path string
	data map[string]interface{}
}

type Value string

func (c *Content) StringData(name string) (string, error) {
	raw, ok := c.data[name]
	if !ok {
		return "", errVarNotExist
	}

	if val, ok := raw.(Value); ok {
		ret, err := cfg.env.Eval(string(val))
		if err != nil {
			return "", err
		}

		if str, ok := ret.(string); !ok {
			return "", fmt.Errorf("variable '%s' is not a string", name)
		} else {
			return str, nil
		}
	}

	if str, ok := raw.(string); !ok {
		return "", fmt.Errorf("variable '%s' is not a string", name)
	} else {
		return str, nil
	}
}

func (t *Content) Filename() string {
	return path.Base(t.Target())
}

func (t *Content) RelTarget() string {
	return strings.TrimPrefix(t.Target(), cfg.dirs.output+"/")
}

func (t *Content) Target() string {
	relname := strings.TrimPrefix(t.path, cfg.dirs.content)

	filename, err := t.StringData("filename")
	switch err {
	case nil:
		relname = path.Join(path.Dir(relname), filename)
	case errVarNotExist:
		if strings.HasSuffix(relname, ".md") {
			relname = strings.TrimSuffix(relname, ".md") + ".html"
		}
	default:
		fail("     - Error: %s\n", err)
	}

	return path.Join(cfg.dirs.output, relname)
}

func (t *Content) IsBasic() bool {
	return t.data == nil
}

func (t *Content) Template() string {
	template, err := t.StringData("template")
	switch err {
	case nil:
		return template
	case errVarNotExist:
		fail("     - Error: template variable is mandatory")
	default:
		fail("     - Error: %s\n", err)
	}
	panic("never")
}

func ReadData(path string, base map[string]interface{}) (bin []byte, data map[string]interface{}, err error) {
	f, err := os.Open(path)
	if err != nil {
		return
	}
	defer f.Close()

	r := bufio.NewReader(f)
	data = make(map[string]interface{})
	if base != nil {
		for k, v := range base {
			data[k] = v
		}
	}

	for {
		var line string
		line, err = r.ReadString('\n')
		if err != nil {
			return
		}
		if strings.TrimSpace(line) == "--" {
			break
		}

		parts := strings.SplitN(line, ":", 2)
		if len(parts) != 2 {
			return nil, nil, errMeta
		}
		data[strings.TrimSpace(parts[0])] = Value(parts[1])
	}

	bin, err = ioutil.ReadAll(r)
	return
}

func ReadConfig(path string) map[string]interface{} {
	_, data, err := ReadData(path, nil)
	if err != nil && os.IsNotExist(err) {
		return nil
	}

	fmt.Printf("    config          %s\n", path)
	if err != nil {
		fail("     - Error: %s\n", err)
	}
	return data
}

func ReadContent() ([]*Content, map[string]interface{}) {
	fmt.Println("Reading contents")
	cfile := path.Join(cfg.dirs.content, "CONFIG")
	basedata := ReadConfig(cfile)

	content := make([]*Content, 0, 100)
	filevars := make(map[string]interface{})
	err := filepath.Walk(cfg.dirs.content, func(current string, info os.FileInfo, err error) error {
		if err != nil {
			return nil
		}
		if cfile == current {
			return nil
		}

		target := path.Clean(path.Join(cfg.dirs.output, current[len(cfg.dirs.content):]))

		// create directories
		if info.IsDir() {
			fmt.Printf("    directory       %s\n", current)
			err := os.Mkdir(target, info.Mode().Perm())
			if err != nil && os.IsExist(err) {
				return nil
			} else {
				return err
			}
		}

		// copy symlinks
		if info.Mode()&os.ModeSymlink != 0 {
			fmt.Printf("    symlink         %s\n", current)
			link, err := os.Readlink(current)
			if err != nil {
				return err
			}
			err = os.Symlink(link, target)
			return err
		}

		// skip special files
		if !info.Mode().IsRegular() {
			fmt.Printf("    special         %s\n", current)
			return nil
		}

		switch path.Ext(current) {
		case ".var":
			fmt.Printf("    variable file   %s\n", current)
			data, err := ioutil.ReadFile(current)
			if err != nil {
				return err
			}
			if strings.HasSuffix(current, ".md.var") {
				filevars[current] = template.HTML(blackfriday.MarkdownCommon(data))
			} else if strings.HasSuffix(current, ".html.var") {
				filevars[current] = template.HTML(data)
			} else {
				filevars[current] = string(data)
			}
		case ".md":
			fmt.Printf("    markdown        %s\n", current)
			bin, data, err := ReadData(current, basedata)
			if err != nil {
				return err
			}
			data["content"] = template.HTML(string(blackfriday.MarkdownCommon(bin)))
			content = append(content, &Content{path: current, data: data})
		case ".html":
			fmt.Printf("    html            %s\n", current)
			bin, data, err := ReadData(current, basedata)
			if err != nil {
				return err
			}
			data["content"] = template.HTML(string(bin))
			content = append(content, &Content{path: current, data: data})
		default:
			fmt.Printf("    basic file      %s\n", current)
			content = append(content, &Content{path: current, data: nil})
			return Copy(current, target)
		}

		return nil
	})

	if err != nil {
		fail("     - Error: %s\n", err)
	}
	return content, filevars
}
