package main

import (
	"fmt"
	"os"
	"path"
)

func InitOutput() {
	fmt.Println("Initialize output directory")
	fmt.Println("    Removing existing files")
	err := os.RemoveAll(cfg.dirs.output)
	if err != nil {
		fail("      - error: %s\n", err)
	}

	fmt.Println("    Create directory")
	err = os.Mkdir(cfg.dirs.output, 0750)
	if err != nil {
		fail("      - error: %s\n", err)
	}

	assets := path.Join(cfg.dirs.template, "assets")
	if _, err = os.Stat(assets); err == nil {
		fmt.Println("    Copy static assets")
		err = CopyAll(assets, cfg.dirs.output)
		if err != nil {
			fail("      - error: %s\n", err)
		}
	} else {
		fmt.Println("    No static assets in template")
	}
}
