package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/bloatgen/bloatgen/value"
	"os"
	"path"
	"strings"
)

func List(input string) []string {
	out := strings.Split(input, ",")
	for i, _ := range out {
		out[i] = strings.TrimSpace(out[i])
	}
	return out
}

func Json(input string) interface{} {
	var v interface{}
	err := json.Unmarshal([]byte(input), &v)
	if err != nil {
		fail("     - error: %s\n", err)
	}
	return v
}

func Filevar(input string) interface{} {
	ret, ok := cfg.vars[path.Join(cfg.dirs.content, input)]
	if !ok {
		fail("     - error: non existent variable file %s\n", input)
	}
	return ret
}

func ListContent(pattern string, args ...string) []map[string]interface{} {
	v := make([]map[string]interface{}, 0, 0)
	for _, c := range cfg.content {
		match, err := path.Match(pattern, c.RelTarget())
		if err != nil {
			fail("     - error: %s\n", err)
		}
		if !match {
			continue
		}

		entry := make(map[string]interface{})
		entry["path"] = c.RelTarget()
		for _, arg := range args {
			if val, ok := c.data[arg].(Value); ok {
				entry[arg], err = cfg.env.Eval(string(val))
				if err != nil {
					fail("     - error in %s: %s\n", c.path, err)
				}
			} else {
				entry[arg] = c.data[arg]
			}
		}

		v = append(v, entry)
	}
	return v
}

func InitEnvironment() *value.Environment {
	e := value.NewEnvironment()
	e.SetSymbol("list", List)
	e.SetSymbol("json", Json)
	e.SetSymbol("filevar", Filevar)
	e.SetSymbol("content", ListContent)
	return e
}

// Preprocess special variables
func (c *Content) ProcessVars() map[string]interface{} {
	pvars := make(map[string]interface{})
	for key, value := range c.data {
		if val, ok := value.(Value); ok {
			if ret, err := cfg.env.Eval(string(val)); err != nil {
				fail("     - error: %s\n", err)
			} else {
				pvars[key] = ret
			}
		} else {
			pvars[key] = value
		}
	}
	return pvars
}

func Render() {
	fmt.Println("Rendering output")
	for _, this := range cfg.content {
		if this.IsBasic() {
			continue
		}
		pvars := this.ProcessVars()

		fmt.Printf("    %s\n", this.Target())
		out, err := os.Create(this.Target())
		if err != nil {
			fail("     - error: %s\n", err)
		}

		err = cfg.template.ExecuteTemplate(out, this.Template(), pvars)
		if err != nil {
			fail("     - error: %s\n", err)
		}
	}
}
