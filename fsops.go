package main

import (
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
)

type SkippedError []string

func (se SkippedError) Error() string {
	msg := fmt.Sprintf("Skipped non regular files: %s", se[0])
	for i := 1; i < len(se); i++ {
		msg += fmt.Sprintf(", %s", se[i])
	}
	return msg
}

func Copy(src, dst string) error {
	srcFile, err := os.Open(src)
	if err != nil {
		return err
	}
	defer srcFile.Close()

	dstFile, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer dstFile.Close()

	_, err = io.Copy(dstFile, srcFile)
	return err
}

func CopyAll(src, dst string) error {
	skipped := make(SkippedError, 0, 0)
	src = path.Clean(src)
	dst = path.Clean(dst)

	err := filepath.Walk(src, func(current string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		target := path.Join(dst, current[len(src):])

		// create directories
		if info.IsDir() {
			err := os.Mkdir(target, info.Mode().Perm())
			if err != nil && os.IsExist(err) {
				return nil
			}
			return err
		}

		// Copy symlinks
		if info.Mode()&os.ModeSymlink != 0 {
			link, err := os.Readlink(current)
			if err != nil {
				return err
			}
			err = os.Symlink(link, target)
			return err
		}

		// skip special files
		if !info.Mode().IsRegular() {
			skipped = append(skipped, current)
			return nil
		}

		// copy regular files
		return Copy(current, target)
	})

	switch {
	case err != nil:
		return err
	case len(skipped) != 0:
		return skipped
	default:
		return nil
	}
}
