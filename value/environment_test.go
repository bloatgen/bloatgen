package value

import (
	"fmt"
	"strconv"
	"testing"
)

func ToInt(in string) int {
	out, _ := strconv.Atoi(in)
	return out
}

func Add(args ...int) int {
	val := 0
	for _, arg := range args {
		val += arg
	}
	return val
}

func TestEnv(t *testing.T) {
	e := NewEnvironment()
	e.SetSymbol("int", ToInt)
	e.SetSymbol("add", Add)
	e.SetSymbol("concat", fmt.Sprintln)

	ret, err := e.Eval(`    just a simple string    `)
	if err != nil {
		t.Log("unexpected error: ", err)
		t.Fail()
	} else if str, ok := ret.(string); !ok {
		t.Log("Unexpected return type")
		t.Fail()
	} else if str != "just a simple string" {
		t.Logf("Unexpected return value '%s'", str)
		t.Fail()
	}

	ret, err = e.Eval(`concat("5 + 6 =", add(int('5'), int('6')))`)
	if err != nil {
		t.Log("unexpected error: ", err)
		t.Fail()
	} else if str, ok := ret.(string); !ok {
		t.Log("Unexpected return type")
		t.Fail()
	} else if str != "5 + 6 = 11\n" {
		t.Logf("Unexpected return value '%s'", str)
		t.Fail()
	}
}
