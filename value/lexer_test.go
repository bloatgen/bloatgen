package value

import (
	"testing"
)

func TestLex(t *testing.T) {
	ls, err := lexAll("name('l''it', \"l\"\"it\", `l``it`)")
	if err != nil {
		t.Fatal("unexpected lex error:", err)
	}

	expected := []lexeme{
		lexeme{tokName, "name"},
		lexeme{tokLParent, "("},
		lexeme{tokLiteral, "l'it"},
		lexeme{tokComma, ","},
		lexeme{tokLiteral, "l\"it"},
		lexeme{tokComma, ","},
		lexeme{tokLiteral, "l`it"},
		lexeme{tokRParent, ")"},
		lexeme{tokEOF, ""},
	}

	if len(ls) != len(expected) {
		t.Fatalf("Expected %d lexemes but got %d", len(expected), len(ls))
	}

	for i, _ := range ls {
		if ls[i].kind != expected[i].kind {
			t.Fatalf("lexeme #%d: expected kind %d but got %d", i, expected[i].kind, ls[i].kind)
		}
		if ls[i].value != expected[i].value {
			t.Fatalf("lexeme #%d: expected value '%s' but got '%s'", i, expected[i].value, ls[i].value)
		}
	}
}
