package value

import (
	"fmt"
	"strings"
	"unicode"
	"unicode/utf8"
)

const (
	tokLiteral = iota
	tokLParent
	tokRParent
	tokComma
	tokName
	tokEOF
)

type lexeme struct {
	kind  int
	value string
}

func consumeWhitespace(line string) (string, error) {
	n := 0
	for _, r := range line {
		if r == utf8.RuneError {
			return "", fmt.Errorf("invalid utf8 rune near '%s'", line[n:])
		}
		if !unicode.IsSpace(r) {
			break
		}
		n += utf8.RuneLen(r)
	}
	return line[n:], nil
}

func consumeLiteral(end rune, line string) (l lexeme, remaining string, err error) {
	n := 0
	haveEnd := false
	for _, r := range line {
		if haveEnd && r != end {
			break
		}

		n += utf8.RuneLen(r)

		if !haveEnd && r == end {
			haveEnd = true
			continue
		}

		if haveEnd && r == end {
			haveEnd = false
		}

		l.value += fmt.Sprintf("%c", r)
	}

	l.kind = tokLiteral
	remaining = line[n:]
	return
}

func isNameFirstRune(r rune) bool {
	return r >= 'a' && r <= 'z' || r >= 'A' && r <= 'Z' || r == '_'
}
func isNameRune(r rune) bool {
	return r >= 'a' && r <= 'z' || r >= 'A' && r <= 'Z' || r >= '0' && r <= '9' || r == '_'
}

func consumeName(line string) (l lexeme, remaining string, err error) {
	n := 0
	for _, r := range line {
		if r == utf8.RuneError {
			err = fmt.Errorf("invalid utf8 rune near '%s'", line[n:])
			return
		}

		if !isNameRune(r) {
			break
		}

		n += utf8.RuneLen(r)
	}
	l.kind = tokName
	l.value = line[:n]
	remaining = line[n:]
	return
}

func lex(line string) (l lexeme, remaining string, err error) {
	r, _ := utf8.DecodeRuneInString(line)
	switch {
	case len(line) == 0:
		l.kind = tokEOF
		return
	case unicode.IsSpace(r):
		line, err = consumeWhitespace(line)
		if err != nil {
			return
		}
		return lex(line)
	case r == ',':
		l.value = ","
		l.kind = tokComma
		remaining = line[1:]
		return
	case r == '(':
		l.value = "("
		l.kind = tokLParent
		remaining = line[1:]
		return
	case r == ')':
		l.value = ")"
		l.kind = tokRParent
		remaining = line[1:]
		return
	case r == '\'' || r == '"' || r == '`':
		return consumeLiteral(r, line[1:])
	case isNameFirstRune(r):
		return consumeName(line)
	default:
		err = fmt.Errorf("unexpected character near '%s'", line)
		return
	}
	panic("nope")
}

func lexAll(line string) ([]lexeme, error) {
	var (
		remainder string
		l         lexeme
		ls        []lexeme
		err       error
	)
	ls = make([]lexeme, 1, 2)

	// If the first lexeme is an error, or if the first lexeme is NOT a name
	// and NOT a literal then we treat the whole line as if it was a literal
	ls[0], remainder, err = lex(line)
	if err != nil || (ls[0].kind != tokName && ls[0].kind != tokLiteral) {
		ls[0].kind = tokLiteral
		ls[0].value = strings.TrimSpace(line)
		remainder = ""
	}

	// If the first lexeme is a name, and the second is not an LParent then we
	// treat the whole line as if it was literal
	if ls[0].kind == tokName {
		ls = ls[:2]
		ls[1], remainder, err = lex(remainder)
		if err != nil || ls[1].kind != tokLParent {
			ls[0].kind = tokLiteral
			ls[0].value = strings.TrimSpace(line)
			ls = ls[:1]
			remainder = ""
		}
	}

	for ls[len(ls)-1].kind != tokEOF {
		l, remainder, err = lex(remainder)
		if err != nil {
			return nil, err
		}
		ls = append(ls, l)
	}
	return ls, nil
}
