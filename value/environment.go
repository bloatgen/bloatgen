package value

import (
	"fmt"
	"reflect"
)

type Environment struct {
	symbols map[string]interface{}
}

func NewEnvironment() *Environment {
	return &Environment{make(map[string]interface{})}
}

func (e *Environment) SetSymbol(name string, fn interface{}) {
	ft := reflect.TypeOf(fn)
	if ft.Kind() != reflect.Func {
		panic("invalid symbol: not a function")
	}
	if ft.NumOut() > 1 {
		panic("Symbols may only return 0 or 1 arguments.")
	}
	e.symbols[name] = fn
}

func (e *Environment) Eval(value string) (interface{}, error) {
	a, err := parse(value)
	if err != nil {
		return nil, err
	}

	return a.value(e)
}

func (e *Environment) call(name string, args []interface{}) (interface{}, error) {
	fn, ok := e.symbols[name]
	if !ok {
		return nil, fmt.Errorf("call to non existing function '%s'", name)
	}

	fv := reflect.ValueOf(fn)

	ins := make([]reflect.Value, 0, len(args))
	for _, arg := range args {
		ins = append(ins, reflect.ValueOf(arg))
	}

	ret := fv.Call(ins)

	if len(ret) == 0 {
		return nil, nil
	} else {
		return ret[0].Interface(), nil
	}
}
