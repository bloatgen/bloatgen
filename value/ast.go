package value

type function struct {
	name string
	args []ast
}

type ast struct {
	ast interface{}
}

func (a ast) value(e *Environment) (interface{}, error) {
	// We're a function
	if fn, ok := a.ast.(function); ok {
		// So we get the value of all our arguments
		args := make([]interface{}, 0, 0)
		for _, arg := range fn.args {
			rval, err := arg.value(e)
			if err != nil {
				return nil, err
			}
			args = append(args, rval)
		}
		// And then call the function
		return e.call(fn.name, args)
	} else {
		// We're not a function, so return us as is
		return a.ast, nil
	}
}
