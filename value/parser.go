package value

import (
	"fmt"
)

type parser struct {
	ls []lexeme
}

// Checks if a sequence of lexemes are in the list
func (p *parser) checkAll(kinds ...int) error {
	for i, kind := range kinds {
		if p.ls[i].kind != kind {
			return fmt.Errorf("expected %d but got %d", kind, p.ls[i].kind)
		}
	}
	return nil
}

// checks if the top of the list contains any of the given lexemes
func (p *parser) checkAny(kinds ...int) (int, error) {
	for _, kind := range kinds {
		if p.ls[0].kind == kind {
			return p.ls[0].kind, nil
		}
	}

	return p.ls[0].kind, fmt.Errorf("expected any of %v but got %d", kinds, p.ls[0].kind)
}

func (p *parser) pop() lexeme {
	l := p.ls[0]
	p.ls = p.ls[1:]
	return l
}

func (p *parser) literal() (ast, error) {
	if err := p.checkAll(tokLiteral); err != nil {
		return ast{}, err
	}
	lit := p.pop()
	return ast{lit.value}, nil
}

func (p *parser) function() (ast, error) {
	var fn function

	if err := p.checkAll(tokName, tokLParent); err != nil {
		return ast{}, err
	}
	fn.name = p.pop().value
	fn.args = make([]ast, 0, 0)
	p.pop()

	if p.checkAll(tokRParent) == nil {
		p.pop()
		return ast{fn}, nil
	}

	for {
		arg, err := p.expression()
		if err != nil {
			return ast{}, err
		}
		fn.args = append(fn.args, arg)

		found, err := p.checkAny(tokComma, tokRParent)
		if err != nil {
			return ast{}, err
		}

		p.pop()
		if found == tokRParent {
			break
		}
	}

	return ast{fn}, nil
}

func (p *parser) expression() (ast, error) {
	found, err := p.checkAny(tokLiteral, tokName)
	if err != nil {
		return ast{}, err
	}

	switch found {
	case tokLiteral:
		return p.literal()
	case tokName:
		return p.function()
	}
	panic("oops")
}

func parse(line string) (ast, error) {
	ls, err := lexAll(line)
	if err != nil {
		return ast{}, err
	}

	p := parser{ls}
	a, err := (&p).expression()
	if err != nil {
		return ast{}, err
	}

	return a, p.checkAll(tokEOF)
}
