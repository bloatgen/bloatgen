package main

import (
	"fmt"
	"gitlab.com/bloatgen/bloatgen/value"
	"html/template"
	"os"
	"path/filepath"
)

var cfg struct {
	dirs struct {
		content  string
		template string
		output   string
	}
	template *template.Template
	env      *value.Environment
	vars     map[string]interface{}
	content  []*Content
}

func fail(format string, args ...interface{}) {
	fmt.Printf(format, args...)
	os.Exit(1)
}

func parseArgs() (cDir, tDir, oDir string) {
	if len(os.Args) != 4 {
		fail("Usage: %s <content directory> <template directory> <output directory>\n", os.Args[0])
	}

	var err error
	if cDir, err = filepath.Abs(os.Args[1]); err != nil {
		fail("%s\n", err)
	}
	if tDir, err = filepath.Abs(os.Args[2]); err != nil {
		fail("%s\n", err)
	}
	if oDir, err = filepath.Abs(os.Args[3]); err != nil {
		fail("%s\n", err)
	}
	return
}

func main() {
	cfg.env = InitEnvironment()
	cfg.dirs.content, cfg.dirs.template, cfg.dirs.output = parseArgs()
	cfg.template = ReadTemplate()
	InitOutput()
	cfg.content, cfg.vars = ReadContent()
	Render()
}
